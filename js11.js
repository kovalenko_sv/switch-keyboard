"use strict";

document.addEventListener("keypress", colorize);

function colorize(event) {

    const key = event.key;

    const button = [...document.querySelectorAll(".btn")];

    [...document.querySelectorAll(".btn")].forEach(el => {
        let btnName = el.getAttribute("data-key");

        if (btnName === key) {
            el.classList.add("active")
        } else {
            el.classList.remove("active")
        };
    });
}